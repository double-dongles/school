
> 1+2+3+4+...+10

This is clearly too long and inefficient.

So, we use the Sigma notation

$$\sum_{i=1}^{10} i$$

$\Sigma$ is the sigma

$_{i = 1}$ means that we initialize a variable of i=1, and increment by one

$^{10}$ means that increment 1 until 10
